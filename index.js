function countLetter(letter, sentence) {
    
    let result = 0;


    if(letter.length==1){
        let checkEachLetter = sentence.split("");
            for (let i = 0; i < checkEachLetter.length; i++) {
                  if (letter.indexOf(checkEachLetter[i]) > -1) {
                        result++;
                  }
            } 
    }else{   
      result=undefined;    
    }
    return result;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {

let isIsogram=true;
let eachLetter=text.toLowerCase();
    eachLetter=eachLetter.split("");
    for (let i = 0; i < eachLetter.length; i++) {
        for (let y = 0; y < eachLetter.length; y++) {   
                if(i!=y){
                    if(eachLetter[i]==eachLetter[y]){
                             isIsogram=false;
                             break;
                    }
                }
        }
    }
    return isIsogram;
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
}

function purchase(age, price) {
    let result="";
    let priceDiscount=price * 0.8;
    if(age<13){
         result=undefined;
    } else if(age>=13 && age<=21){
            result=`${priceDiscount.toFixed(2)}`;            
    }else if(age>=22&& age<=64){
        result=`${price.toFixed(2)}`;  
    }else{
          result=`${priceDiscount.toFixed(2)}`;  
    }
    
return result;
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
let itemNoStock=[];
    items.map(itemStock =>{
        if(itemStock.stocks==0){
                    itemNoStock.push(itemStock.category);    
        }    
     })
let combineSameItem = itemNoStock.filter(function(item, pos) {
    return itemNoStock.indexOf(item) == pos;
})
return combineSameItem;
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let foundFlyingVoter = [];
        for (let i = 0; i < candidateA.length; i++) {
            if (candidateB.indexOf(candidateA[i]) > -1) {
                foundFlyingVoter.push(candidateA[i]);
            }
        } 
    return foundFlyingVoter; 
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};